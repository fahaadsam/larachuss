<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
class Dashboard extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index(){
        $user = DB::table('user')->select('id','name','email')->get();
        //dd($user);
        return view('user/list',['user'=>$user]);
    }
    public function add(){
        return view('user/form');
    }
    public function command(Request $request){
        $req = $request->all();
        dd($req);
        $user = new User;
        $user->name = $req['name'];
        $user->role_id = $req['role_id'];
        $user->email = $req['email'];
        $user->save();
        //return redirect()->back();
    }
}

