@extends('layout.mainlayout') 
@section('content')
<div class="row">
	<a href="{{ url('addUser') }}" class="btn btn-info pull-right">Add New</a>
	<div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Data Table</h3>
            <p class="text-muted m-b-30">Data table example</p>
            <div class="table-responsive">
                <table id="myTable" class="table color-table info-table no-footer dataTable div2">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $user)
                        <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->name }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
@endsection