@extends('layout.mainlayout') 
@section('content')
<div class="row">
    <a href="{{ url('UserList') }}" class="btn btn-info pull-right">List view</a>
    <form class="submit">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">User Name</label>
                                <input type="text" name="username" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Role</label>
                                <select name="role_id" class="form-control" data-placeholder="Choose a Role" tabindex="1">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">5</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="email" name="email" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control"> </div>
                        </div>
                    </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact</label>
                                <input type="text" name="mobile_no" class="form-control"> </div>
                        </div>
                        <!--/span-->
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <button type="button" class="btn btn-default">Cancel</button>
                </div>
                </div>
            </div>
    </form>
@endsection