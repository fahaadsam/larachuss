<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="">
<script type="text/javascript">
var BASEURL         =   '';
var id              =   '';
var currController  =  "";
</script>
<title>Web ERP</title>
<link href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/css/style.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/switchery/dist/switchery.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/multiselect/css/multi-select.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/css/colors/blue.css')}}" id="theme" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css')}}" />
<link href="{{ asset('assets/plugins/bower_components/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/plugins/bower_components/bootstrap-switch/bootstrap-switch.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/css-chart/css-chart.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/sweetalert/sweetalert.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/jquery-datatables-editable/datatables.css')}}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css')}}" />
<link href="{{ asset('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!--<link href="{{ asset('assets/css/jquery.nestable.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/select2.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/jquery-ui.css')}}" rel="stylesheet">-->